from torch.utils.data import Dataset
import numpy as np
import random
import cv2
#import h5py
import scipy.io as sio
import os
import matplotlib.pyplot as plt
#import torchvision.transforms as transforms
import torch
from sklearn.model_selection import train_test_split

def normalize_hyperspectral_image(image):
    max_value = np.max(image)
    min_value = np.min(image)
    #normalized_image = (image - min_value) / (max_value - min_value) * 2 - 1
    shifted_image = image - min_value
    normalized_image = shifted_image / (max_value - min_value)
    return normalized_image

# def normalize_hyperspectral_image(image):
#     max_value = np.max(image)
#     min_value = np.min(image)
#     normalized_image = (image - min_value) / (max_value - min_value)
#     return normalized_image

class TrainDataset(Dataset):
    def __init__(self, data_root, crop_size, val_split=0.05, arg=True, bgr2rgb=True, stride=8, stage='train'):
        self.crop_size = crop_size
        self.hypers = []
        self.bgrs = []
        self.arg = arg
        h,w = 400, 400  # img shape
        self.stride = stride
        self.patch_per_line = (w-crop_size)//stride+1
        self.patch_per_colum = (h-crop_size)//stride+1
        self.patch_per_img = self.patch_per_line*self.patch_per_colum
        self.val_split = val_split
        self.bgr2rgb = bgr2rgb
        self.stage = stage
        self.init = False

        self.hyper_data_path = f'{data_root}/norm/test/'
        # self.hyper_data_path = f'{data_root}/vqvae/'

        self.bgr_data_path = f'{data_root}/ENMAP_rgb/'

        self.hyper_list = [os.path.basename(file_path) for file_path in os.listdir(self.hyper_data_path)]
        self.hyper_list.sort()      
        
        print(f'len(total hypers) of ENMAP dataset:{len(self.hyper_list)}')

        # for i in range(len(self.hyper_list)):
            # print(f'hyper_list[{i}]:{self.hyper_list[i]}')

        # self.img_num = len(self.train_paths)
        self.patch_per_image = 1
        # self.length = self.img_num
        hyper_path = self.hyper_data_path + self.hyper_list[0]
        mat = sio.loadmat(hyper_path)
        hyper = np.float32(np.array(mat['cube']))
        self.hyper = []
        self.hyper.append(hyper.clip(min=0))
        
        hyper_path = self.hyper_data_path + self.hyper_list[1]
        mat = sio.loadmat(hyper_path)
        hyper = np.float32(np.array(mat['cube']))
        self.hyper.append(hyper.clip(min=0))
        
        
        flattened_array = self.hyper[0].reshape(-1, hyper.shape[0])
        flatten_array2 = self.hyper[1].reshape(-1, hyper.shape[0])
        self.flattened_array = np.append(flattened_array, flatten_array2, axis=0)
        
        print(f'size(self.flattened_array): {self.flattened_array.shape}')        

        # self.train_data, self.val_data = train_test_split(self.flattened_array, test_size=self.val_split, random_state=42)        
        # self.train_data = self.flattened_array

    def arguement(self, img, rotTimes, vFlip, hFlip):
        # Random rotation
        for j in range(rotTimes):
            img = np.rot90(img.copy(), axes=(1, 2))
        # Random vertical Flip
        for j in range(vFlip):
            img = img[:, :, ::-1].copy()
        # Random horizontal Flip
        for j in range(hFlip):
            img = img[:, ::-1, :].copy()
        return img
    
    def hsiarguement(self, img, rotTimes, vFlip, hFlip):
        # Random rotation
        for j in range(rotTimes):
            img = np.rot90(img.copy(), axes=(2, 1))
        # Random vertical Flip
        for j in range(vFlip):
            img = img[:, ::-1, : ].copy()
        # Random horizontal Flip
        for j in range(hFlip):
            img = img[:, :, ::-1].copy()
        return img

    def __getitem__(self, idx):
        hyper = self.flattened_array[idx]

        return np.ascontiguousarray(hyper)
    
    def __len__(self):
        return len(self.flattened_array)

class ValidDataset(Dataset):
    def __init__(self, data_root, crop_size, val_split=0.05, arg=True, bgr2rgb=True, stride=8, stage='train'):
        self.crop_size = crop_size
        self.hypers = []
        self.bgrs = []
        self.arg = arg
        h,w = 400, 400  # img shape
        self.stride = stride
        self.patch_per_line = (w-crop_size)//stride+1
        self.patch_per_colum = (h-crop_size)//stride+1
        self.patch_per_img = self.patch_per_line*self.patch_per_colum
        self.val_split = val_split
        self.bgr2rgb = bgr2rgb
        self.stage = stage
        self.init = False

        self.hyper_data_path = f'{data_root}/norm/test/'
        # self.hyper_data_path = f'{data_root}/vqvae/'

        self.hyper_list = [os.path.basename(file_path) for file_path in os.listdir(self.hyper_data_path)]
        self.hyper_list.sort()
                       
        print(f'len(total hypers) of ENMAP dataset:{len(self.hyper_list)}')

        # for i in range(len(self.hyper_list)):
            # print(f'hyper_list[{i}]:{self.hyper_list[i]}')

        # self.img_num = len(self.train_paths)
        self.patch_per_image = 1
        # self.length = self.img_num
        hyper_path = self.hyper_data_path + self.hyper_list[0]
        mat = sio.loadmat(hyper_path)
        hyper = np.float32(np.array(mat['cube']))
        self.hyper = []
        self.hyper.append(hyper.clip(min=0))
        
        hyper_path = self.hyper_data_path + self.hyper_list[1]
        mat = sio.loadmat(hyper_path)
        hyper = np.float32(np.array(mat['cube']))
        # self.hyper = hyper.clip(min=0)
        self.hyper.append(hyper.clip(min=0))
        
        # self.flattened_array = hyper.reshape(-1, hyper.shape[0])
        
        # print(f'size(self.flattened_array): {self.flattened_array.shape}')        

        # self.train_data, self.val_data = train_test_split(self.flattened_array, test_size=self.val_split, random_state=42)        
        # self.train_data = self.flattened_array
       
    def arguement(self, img, rotTimes, vFlip, hFlip):
        # Random rotation
        for j in range(rotTimes):
            img = np.rot90(img.copy(), axes=(1, 2))
        # Random vertical Flip
        for j in range(vFlip):
            img = img[:, :, ::-1].copy()
        # Random horizontal Flip
        for j in range(hFlip):
            img = img[:, ::-1, :].copy()
        return img
    
    def hsiarguement(self, img, rotTimes, vFlip, hFlip):
        # Random rotation
        for j in range(rotTimes):
            img = np.rot90(img.copy(), axes=(2, 1))
        # Random vertical Flip
        for j in range(vFlip):
            img = img[:, ::-1, : ].copy()
        # Random horizontal Flip
        for j in range(hFlip):
            img = img[:, :, ::-1].copy()
        return img

    def __getitem__(self, idx):
        hyper = self.hyper[idx]
        stride = self.stride
        crop_size = self.crop_size

        h_h, w_h = hyper.shape[1], hyper.shape[2]
        
        # Calculate the maximum valid top-left corner for random crop
        max_top = h_h - crop_size
        max_left = w_h - crop_size

        # Generate random top-left corner coordinates for crop
        top = random.randint(0, max_top)
        left = random.randint(0, max_left)

        # Crop the images using the random coordinates
        hyper = hyper[:, left:left + crop_size, top:top + crop_size]

        rotTimes = random.randint(0, 3)
        vFlip = random.randint(0, 1)
        hFlip = random.randint(0, 1)

        hyper = self.hsiarguement(hyper, rotTimes, vFlip, hFlip)

        return np.ascontiguousarray(hyper)
    
    def __len__(self):
        return len(self.hyper)
        
if __name__ == '__main__':
    train_loader = TrainDataset(data_root='/media/ct00659/scratch', crop_size=128, stage='train', bgr2rgb=True, arg=True)
    valid = ValidDataset(data_root='/media/ct00659/scratch', crop_size=128, bgr2rgb=False)
    hyper1 = train_loader[1]
    hyper2 = valid[0]
    
    print('len(train_loader):', len(train_loader))
    print('len(valid):', len(valid))
    
    print(f'train_shape {hyper1.shape}')
    print(f'val_shape {hyper2.shape}')

    # # print(hyper1)
    # print('hyper1')
    # print(HSI_data)
    # print('hyper valid')
    # print(bgr1)
    # print(('bgr1'))
    # print(bgr)
    # print(f'length of train_loader:{len(train_loader)}')
    # print(f'length of valid_loader:{len(valid)}')
    
    # # HSI_data = HSI_data.transpose(1, 2, 0)
    # # HSI_data = np.transpose(HSI_data, [1, 2, 0])
    # print(bgr1.shape)
    # bgr1 = bgr1.transpose(2, 1, 0)
    
    # print(hyper1.shape)
    # print(bgr1.shape)
    
    # #UNNORMALIZE the hyperspectral image and then convert to a RGB image
    # # data_norm = (HSI_data - int(HSI_data.min())) / (int(HSI_data.max()) - int(HSI_data.min()))
    # max_values  = np.load(os.path.join('/media/ct00659/scratch/', 'norm/max_values.npy'))
    # min_values  = np.load(os.path.join('/media/ct00659/scratch/', 'norm/min_values.npy'))
    
    # channels, height, width = hyper1.shape
    # # breakpoint()
    # unnormalized_image = np.zeros((channels, height, width))
    
    # for channel in range(max_values.shape[-1]):
    #     unnormalized_image[channel] = (hyper1[channel] * (max_values[channel] - min_values[channel])) + min_values[channel]
    
    # data_norm = (unnormalized_image - int(unnormalized_image.min())) / (int(unnormalized_image.max()) - int(unnormalized_image.min())) 
    
    # data_norm = np.round(data_norm * 255).astype(np.uint8)
    # print(data_norm.shape)

    # # data_norm = np.round(data_norm * 255).astype(np.uint8)
    # data_norm = np.clip(data_norm, 0, None)
    # rot_image = np.zeros((unnormalized_image.shape[1], unnormalized_image.shape[2], 3), dtype=np.uint8)
    # # Create an rgb image from the crop - use shape of pre-normalized image to get correct dimensions
    # rot_image[:, :, 0] = data_norm[47] #R
    # rot_image[:, :, 1] = data_norm[24] #G
    # rot_image[:, :, 2] = data_norm[9] #B

    # # plt.imshow(rot_image)
    
    # #plot the two rgb and converted rgv image to see
    # fig, (ax1, ax2) = plt.subplots(1, 2)
    # ax1.imshow(bgr1)
    # ax1.set_title('Input image')
    # ax2.imshow(rot_image)
    # ax2.set_title('Reconstructed image')
    
    # plt.show()
    
    # for i, (images, labels, mask) in enumerate(valid):
    #     print(labels.shape, images.shape)
    #     print(labels.max(), labels.min())
    #     print(images.max(), images.min()) 
    #     exit()
        
    # bgr = np.transpose(bgr, [1,2,0])  # [3,482,512] c,h,w
    # fig, (ax1, ax2) = plt.subplots(1, 2)
    # ax1.imshow(bgr, cmap='gray')
    # ax1.set_title('Input image')
    # data_norm = (HSI_data - int(HSI_data.min())) / (int(HSI_data.max()) - int(HSI_data.min()))    
    # data_norm = np.round(data_norm * 255).astype(np.uint8)
    # data_norm = np.clip(data_norm, 0, None)
    # # Create an rgb image from the crop - use shape of pre-normalized image to get correct dimensions
    # rot_image = np.zeros((HSI_data.shape[1], HSI_data.shape[2], 3), dtype=np.uint8)
    # rot_image[:, :, 0] = data_norm[9] #B
    # rot_image[:, :, 1] = data_norm[24] #G
    # rot_image[:, :, 2] = data_norm[47] #R  
    # ax2.imshow(rot_image, cmap='gray')
    # ax2.set_title('Reconstructed image')
    # plt.show()
    
